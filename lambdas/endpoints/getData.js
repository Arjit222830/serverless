const sql = require('../common/SQL');
const Responses = require('../common/API_Responses');

exports.handler = async (event)=>{

    return Responses._200(await sql("SELECT * FROM DATA"));

}