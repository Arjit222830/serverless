const AWS = require('aws-sdk');
const db = require('../../db');
const rdsDataService = new AWS.RDSDataService();

module.exports = async (query)=>{

    const sqlParams = {
        sql: query,
        ...db
    }

    return await rdsDataService.executeStatement(sqlParams).promise();
}