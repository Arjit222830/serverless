/**
 * Exporting common DB configurations.
 */
 module.exports = {
  secretArn: 'arn:aws:secretsmanager:us-east-1:560189291523:secret:rds-db-credentials/cluster-74BJHINVUUGBANGU2MC5PYXV2M/Arjit-pu3C8S',
  resourceArn: 'arn:aws:rds:us-east-1:560189291523:cluster:serverlessdemo',
  database: 'serverlessDemo',
  includeResultMetadata: true,
  resultSetOptions: { 
      decimalReturnType: "DOUBLE_OR_LONG"
  },
}